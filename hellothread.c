#include <pthread.h>
#include <stdio.h>

//void * HelloWorld (void *a)
void* HelloWorld ()
{
    printf (" Hello World \n ");
    return NULL ;
}

int main ()
{
    int MAX = 500;
    pthread_t thread_ids [ MAX ];

    int i;
    for (i = 0; i < MAX; ++i) {
        pthread_create ( & thread_ids [i] , /* returned thread ids */
                        NULL , /* default attributes */
                        & HelloWorld , /* start routine */
                        NULL ); /* argument */
    }

    return 0;
}
