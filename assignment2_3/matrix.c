/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  matrix.c
 *
 *  The parallel calculation is implemented here.
 *
 *  The "calculate_row_sums" function is called from assign2_3.c.
 */

#include <stdio.h>
#include <stdlib.h>

#include "omp.h"

#include "matrix.h"


/* Initialize and fill a triangular matrix with A_ij = i + j
 * and entries only existent for i + j < n <=> j < n - i
 * with n := matrix_size */
int** matrix_init(const int matrix_size) {
    int i, j;

    /* Allocate outer pointer. */
    int** matrix_pptr = matrix_pptr = malloc(matrix_size * sizeof(int*));

    if (!matrix_pptr) {
        fprintf(stderr, "Could not allocate enough memory, aborting.\n");
        exit(EXIT_FAILURE);
    }

    /* Each subsequent pointer needs one less entry */
    for (i = 0; i < matrix_size; ++i) {
        matrix_pptr[i] = malloc((matrix_size - i) * sizeof(int));

        if (!matrix_pptr[i]) {
            fprintf(stderr, "Could not allocate enough memory, aborting.\n");
            exit(EXIT_FAILURE);
        }
    }

    /* Now fill each entry. Note that since our representation of the upper
     * triangular matrix is a lower triangular twodimensional array
     * in the antidiagonal/skew diagonal, index j of our representation is
     * not the same as index j of the matrix.
     *
     * Therefore j starts at i (A_ij is the diagonal) and the array is
     * accessed from zero upwards, which is j - i
     */
    for (i = 0; i < matrix_size; ++i)
        for (j = i; j < matrix_size; ++j)
            matrix_pptr[i][j - i] = i + j;

    return matrix_pptr;
}


void matrix_delete(int** matrix_pptr, const int matrix_size) {
    int i;

    for (i = 0; i < matrix_size; ++i)
        free(matrix_pptr[i]);

    free(matrix_pptr);
}


/* Calculates the row sums. */
void calculate_row_sums (int** matrix_pptr, const int matrix_size,
                         const int nthreads, int* row_sums, int chunk_size) {
    int i, j, total;
    total = 0;

    /* Set num of threads */
    omp_set_num_threads(nthreads);

    /* Parallelized code. */
	#pragma omp parallel for private(i, j) \
                             reduction(+:total)
    for (i = 0; i < matrix_size; ++i) {
        total = 0;

        for (j = 0; j < matrix_size - i; ++j)
            total += matrix_pptr[i][j];

        row_sums[i] = total;
    }
}


/* Lets each outer loop calculate two sums. */
void calculate_row_sums_folded (int** matrix_pptr, const int matrix_size,
                                const int nthreads, int* row_sums,
                                int chunk_size) {
    int i, j, totalbegin, totalend;
    totalbegin = totalend = 0;

    /* Set num of threads */
    omp_set_num_threads(nthreads);

    /* If the matrix size is uneven, first iteration is handled as
     * normal and the rest of the iterations are even, so the
     * case underneath applies */
    if (matrix_size % 2) {
	    #pragma omp parallel for private(i, j) \
                                 reduction(+:totalbegin,totalend)
        for (i = 0; i < matrix_size / 2 + 1; ++i) {
            totalbegin = totalend = 0;

            if (!i) {
                for (j = 0; j < matrix_size - i; ++j)
                    totalbegin += matrix_pptr[i][j];

                row_sums[i] = totalbegin;
            }
            else {
                for (j = 0; j < matrix_size - i; ++j)
                    totalbegin += matrix_pptr[i][j];

                for (j = 0; j < i; ++j)
                    totalend += matrix_pptr[matrix_size - i][j];

                row_sums[i] = totalbegin;
                row_sums[matrix_size - i] = totalend;
            }
        }
    }
    else {
	    #pragma omp parallel for private(i, j) \
                                 reduction(+:totalbegin,totalend)
        for (i = 0; i < matrix_size / 2; ++i) {
            totalbegin = totalend = 0;

            for (j = 0; j < matrix_size - i; ++j)
                totalbegin += matrix_pptr[i][j];

            for (j = 0; j < i + 1; ++j)
                totalend += matrix_pptr[matrix_size - i - 1][j];

            row_sums[i] = totalbegin;
            row_sums[matrix_size - i - 1] = totalend;
        }
    }
}


void print_results(int** matrix_pptr, int* row_sums, const int matrix_size) {
    int i, j;

    for (i = 0; i < matrix_size; ++i) {
        for (j = 0; j < matrix_size; ++j) {
            if (j < i)
                printf("  ");
            else
                printf("%d ", matrix_pptr[i][j - i]);
        }
        printf("| %d\n", row_sums[i]);
    }
}


