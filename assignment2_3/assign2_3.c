/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  assign2_3.c
 *
 *  Contains code for initializing a matrix and setting up timing.
 *  Based on framework code.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "file.h"
#include "timer.h"
#include "matrix.h"


int main(int argc, char* argv[]) {

    int matrix_size, nthreads, arg_print_results, arg_save_results, chunk_size;
    double time;

    /* Parse commandline args: matrix_size nthreads arg_print_results */
    if (argc < 3) {
        printf("Usage: %s matrix_size nthreads print_results\n", argv[0]);
        printf(" - matrix_size, the size of the upper triangular matrix "
               "should be > 0\n");
        printf(" - nthreads: number of threads to use for simulation, "
                "should be > 0\n");
        printf(" - chunk_size: size of a chunk in the parallel execution, "
                "should be > 0 and default 1\n");
        printf(" - arg_print_results: whether or not to print the results, "
                "default 0\n");
        printf(" - arg_save_results: whether or not to save the results, "
                "default 0\n");

        return EXIT_FAILURE;
    }

    matrix_size = atoi(argv[1]);
    nthreads = atoi(argv[2]);

    if (matrix_size < 1) {
        printf("argument error: matrix_size should be > 0.\n");
        return EXIT_FAILURE;
    }
    if (nthreads < 1) {
        printf("argument error: nthreads should be > 0.\n");
        return EXIT_FAILURE;
    }

    /* Chunk size to a default of 1, also when bad input is given */
    if (argc < 4)
        chunk_size = 1;
    else
        chunk_size =
            (atoi(argv[3]) > 0 ? atoi(argv[3]) : 1);


    /* Don't print or save excessive results... */
    if (argc < 5 || matrix_size > 30)
        arg_print_results = 0;
    else
        arg_print_results = (atoi(argv[4]) ? 1 : 0);

    if (argc < 6 || matrix_size > 30)
        arg_save_results = 0;
    else
        arg_save_results = (atoi(argv[5]) ? 1 : 0);


    int** matrix_pptr = matrix_init(matrix_size);

    int* row_sums = malloc(matrix_size * sizeof(int));
    if (!row_sums) {
        fprintf(stderr, "Could not allocate enough memory, aborting.\n");
        exit(EXIT_FAILURE);
    }


    /* Naive implementation */
    timer_start();

    /* Call the parallel calculation of the row sums */
    calculate_row_sums(matrix_pptr, matrix_size, nthreads,
                       row_sums, chunk_size);

    time = timer_end();
    printf("Naive with matrix size %d, nthreads %d, chunk_size %d:\n",
            matrix_size, nthreads, chunk_size);
    printf("Took %g seconds\n", time);
    printf("Normalized: %g seconds\n", time / matrix_size);


    /* Folded implementation */
    timer_start();

    /* Call the parallel calculation of the row sums */
    calculate_row_sums_folded(matrix_pptr, matrix_size, nthreads,
                              row_sums, chunk_size);


    time = timer_end();
    printf("Folded with matrix size %d, nthreads %d, chunk_size %d:\n",
            matrix_size, nthreads, chunk_size);
    printf("Took %g seconds\n", time);
    printf("Normalized: %g seconds\n", time / matrix_size);


    if (arg_print_results)
        print_results(matrix_pptr, row_sums, matrix_size);

    if (arg_save_results) {
        file_write_int_array("result.txt", row_sums, matrix_size);
        file_write_int_array("resultfolded.txt", row_sums, matrix_size);
    }


    matrix_delete(matrix_pptr, matrix_size);
    free(row_sums);

    return EXIT_SUCCESS;
}

