/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  matrix.h
 *
 *  Macros, struct for argument, and function declarations
 */

#pragma once

int** matrix_init(const int);
void matrix_delete(int**, const int);

void calculate_row_sums (int**, const int, const int, int*, int);
void calculate_row_sums_folded (int**, const int, const int, int*, int);

void print_results(int**, int*, const int);

