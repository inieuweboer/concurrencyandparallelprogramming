/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  file.c
 *
 *  Contains several functions for file I/O.
 *  Based on framework code.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>


/*
 * Saves an array with n items to a given file, overwriting any previous
 * contents.
 */
void file_write_int_array(const char* filename, int* array, int n) {

    FILE* fp;
    int i;

    fp = fopen(filename, "w");

    if (!fp) {
        fprintf(stderr, "Failed to open file %s: %s\n", filename,
                strerror(errno));
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < n; i++) {
        fprintf(fp, "%d\n", array[i]);
    }

    fclose(fp);
}



