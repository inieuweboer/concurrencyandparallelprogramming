/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  file.h
 *
 *  Contains several functions for file I/O.
 *  Based on framework code.
 */

#pragma once

void file_write_int_array(const char*, int*, int n);
