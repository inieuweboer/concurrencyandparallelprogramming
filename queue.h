/*
 *  Ismani Nieuweboer - 10502815
 *  DB Wiskunde en Informatica
 *
 *  Assignment 5: Finding the shortest path.
 *
 *  Implementation of a queue as a linked list.
 */

#include "main.h"

#include "queue.h"

/******************************************************************************/

queue_t* queue_init() {

    queue_t* queue = malloc(sizeof(queue_t) );;
    assert(queue);

    /*  A queue always starts off empty */
    queue->size = 0;

    queue->first_item = NULL;
    queue->last_item = NULL;

    return queue;
}


/*  Pushes data as the last item onto the queue.
 *
 *  Assumes memory is already allocated for the data. */
void enqueue(queue_t* queue, void* data) {

    if(queue == NULL) {
        fprintf(stderr, "Error: Nullpointer in enqueue\n");
        exit(EXIT_FAILURE);
    }

    queue_item_t* new_item = NULL;
    new_item = malloc(sizeof(queue_item_t) );

    new_item->data = data;
    new_item->next_item = NULL;

    /*  If the queue is empty, assign the new item as first item,
     *  and assign the last item pointer to the first item  */
    if(queue->first_item == NULL) {
        queue->first_item = new_item;
        queue->last_item = queue->first_item;
    }
    /*  Otherwise insert after last item, and update the last item pointer */
    else {
        queue->last_item->next_item = new_item;
        queue->last_item = queue->last_item->next_item;
    }

    queue->size++;
}


/*  Pulls data from the first item in the queue
 *
 *  Assumes memory deallocation will happen with the returned data pointer. */
void* dequeue(queue_t* queue) {

    if(queue == NULL) {
        fprintf(stderr, "Error: Nullpointer in dequeue\n");
        exit(EXIT_FAILURE);
    }

    /*  Create pointers to the first item and the data contained by it */
    queue_item_t* obsolete_item = queue->first_item;
    void* data = queue->first_item->data;

    /*  Reassign the pointer in the queue to the next item
     *  of the item to be popped */
    queue->first_item = queue->first_item->next_item;

    /*  Free the item struct and return data pointer */
    free(obsolete_item);
    queue->size--;

    return data;
}


/*  Delete the entire queue. The second argument of this function takes a
 *  function pointer to a function that frees up the data type. */
void queue_delete(queue_t* queue, void (*free_data)(void*) ) {

    if(queue == NULL) {
        fprintf(stderr, "Error: Nullpointer in queue_delete\n");
        exit(EXIT_FAILURE);
    }

    queue_item_t* conductor = queue->first_item;
    queue_item_t* next_item;

    /*  Queue might be empty */
    if(conductor != NULL) {
        /*  Traverse through the queue and delete each item */
        while(conductor->next_item != NULL) {
            /*  First update the pointer to refer to the next node so the
             *  current can get cleaned up */
            next_item = conductor->next_item;

            /*  Then free up memory and pass the pointer to the next node
             *  for the next iteration */

            if(free_data != NULL) {
                free_data(conductor->data);
            }
            free(conductor);

            conductor = next_item;
        }

        /*  Free up the last item in the queue, and the queue itself */
        if(free_data != NULL) {
            free_data(conductor->data);
        }
        free(conductor);
    }

    free(queue);
}

