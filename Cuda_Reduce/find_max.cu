#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "timer.h"

/* Find the maximum value in an array of floats */
float find_max (float* floats, int size) {

	int i;
	float float_max = floats[0];

	for (i = 0; i < size; i ++) {
		float_max = max(float_max, floats[i]);
	}

	return float_max;
}

/* Generate some random floats */
float* gen_floats(int size) {

	float* floats = (float*) malloc (size * sizeof(floats));
	srand(time(NULL));
	for (int i = 0; i < size; i++)
		floats[i] = (float) rand();
	return floats;
}

/* Function that puts the maximum of the global input assigned to this block
 * in the global output on the index equal to the x of this block.
 * Uses a tree-based approach to reduce data, with taking the maximum
 * as reduction operator. This function was adapted from the NVIDIA
 * tutorial on tree-based block reduction found on:
 * 		www.cuvilib.com/Reduction.pdf 
 * by Mark Harris (who was also sourced in the lecture) */
__global__ void reduce(float* global_in, float* global_out, int size) {
	
	extern __shared__ float sdata[];

	/* Set id relative to block, and global id */
	int rel_id = threadIdx.x;
	int abs_id = blockIdx.x * blockDim.x + threadIdx.x;

	/* We need to check if we stay in bounds */
	if (abs_id < size) {

		/* If we are in bounds, each thread should load one element 
		 * from the global data into the memory assigned to this block */
		sdata[rel_id] = global_in[abs_id];
		__syncthreads();

		/* We reduce the data using interleaved addressing, check once again
		 * if we stay within bounds. This part is fully explained in the PDF */
		for(int s = 1; s < blockDim.x && abs_id + s < size; s*=2) {
			
			int index = 2 * s * rel_id;

			if (index < blockDim.x)
				sdata[index] = max(sdata[index], sdata[index + s]);
				/* To avoid a data race it is important to sync the threads
				 * after each 'wave' of reductions */
				__syncthreads();
		}

		/* Finally put the maximum of this block in the global output */
		if (rel_id == 0)
			global_out[blockIdx.x] = sdata[0];
	}
}

/* Unimproved version of the kernel function */
__global__ void reduce2(float* global_in, float* global_out, int size) {
	
	extern __shared__ float sdata[];

	/* Set id relative to block, and global id */
	int rel_id = threadIdx.x;
	int abs_id = blockIdx.x * blockDim.x + threadIdx.x;

	/* We need to check if we stay in bounds */
	if (abs_id < size) {

		/* If we are in bounds, each thread should load one element 
		 * from the global data into the memory assigned to this block */
		sdata[rel_id] = global_in[abs_id];
		__syncthreads();

		/* We reduce the data using interleaved addressing, check once again
		 * if we stay within bounds. This part is fully explained in the PDF */
		for(int s = 1; s < blockDim.x && abs_id + s < size; s *= 2) {
			if (rel_id % (2*s) == 0)
				sdata[rel_id] = max(sdata[rel_id], sdata[rel_id + s]);
				/* To avoid a data race it is important to synch the threads
				 * after each 'wave' of reductions */
				__syncthreads();
		}

		/* Finally put the maximum of this block in the global output */
		if (rel_id == 0)
			global_out[blockIdx.x] = sdata[0];
	}
}

int main (int argc, char* argv[]) {

	int size, threads, blocks, errorcode;
	float CPU_max;
	float GPU_max;
	
	/* Get arguments */
	if (argc == 3) {
		size = atoi(argv[1]);
		threads = atoi(argv[2]);
	}
	else {
		printf("a.out <#Floats> <#Threads per Block>\n");
		exit(1);
	}

	/* Create some floats and calculate their maximum on the CPU */
	float* floats = gen_floats(size);
	timer_start();
		CPU_max = find_max(floats, size);
	printf("CPU Calculates: %f in %f seconds.\n", CPU_max,timer_end());
	
	float* reductions;
	float* cudaFloats;
	float* cudaReductions;

	timer_start();

	/* Allocate room in CUDA for the floats and copy them */
 	if (errorcode = cudaMalloc(&cudaFloats, sizeof(float) * size)) {
		printf("cudaMalloc returned errorcode %d\n", errorcode);
		exit(1);
	}
	if(errorcode = cudaMemcpy(cudaFloats, floats, sizeof(float) * size, cudaMemcpyHostToDevice)) {
			printf("cudaMemcpy returned errorcode %d\n", errorcode);
			exit(1);
	}

	do {		

		/* Calculate block size */
		blocks = size / threads;
		if (size % threads)
			blocks ++;

		/* Allocate memory for the reductions, both on host and on device */
		reductions = (float*) malloc(blocks * sizeof(float));
		if(errorcode = cudaMalloc(&cudaReductions, sizeof(float) * blocks)) {
			printf("cudaMalloc returned errorcode %d\n", errorcode);
			exit(1);
		}

		/* Set the Dimensions for grid and blocks */
		dim3 numBlocks(blocks);
		dim3 numThreads(threads);

		/* Call the kernel */
		reduce <<< numBlocks, numThreads, sizeof(float) * threads >>> (cudaFloats, cudaReductions, size);	

		if(errorcode = cudaDeviceSynchronize()) {
			printf("cudoDeviceSynchronize returned errorcode %d\n", errorcode);
			exit(1);
		}
		
		/* Copy the results back the the host */
		if(errorcode = cudaMemcpy(reductions, cudaReductions, sizeof(float) * blocks, cudaMemcpyDeviceToHost)) {
			printf("cudoMalloc returned errorcode %d\n", errorcode);
			exit(1);
		}
		
		/* Use the amount of blocks as new size */
		size = blocks;

		/* Allocate new space for the floats, and copy the reductions we just found
		 * to the device to use them as new input */
		if(errorcode = cudaMalloc(&cudaFloats, sizeof(float) * size)) {
			printf("cudoMalloc returned errorcode %d\n", errorcode);
			exit(1);
		}
		if(errorcode = cudaMemcpy(cudaFloats, reductions, sizeof(float) * size, cudaMemcpyHostToDevice)) {
			printf("cudoMalloc returned errorcode %d\n", errorcode);
			exit(1);
		}
	}
	while (blocks != 1);
	
	GPU_max = reductions[0];
	printf("GPU calculates: %f in %f seconds\n", GPU_max, timer_end());
}
