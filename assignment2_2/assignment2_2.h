/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  assign2_2a.h
 *
 *	Header for assignment2_2.c
 */

double red_op(double, double);
double* make_vector (int);
double sum (double*, int);
double naieve_reduce (double (fun)(double, double), double*, int, double);
double opti_reduce (double (fun)(double, double), double*, int, double);
void omp_set_num_threads(int);
void print_vector(double* vec, int len);
int main(int, char**);



