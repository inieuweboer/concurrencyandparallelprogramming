/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  assign2_2a.c
 *
 *  Contains a parallelized versions of the reduction-functions from the assignement as well
 *  as a reimplementation of the reduce funtion that is better optimized for multi-threading.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "timer.h"

void omp_set_num_threads(int);

/* A reduction operator. We use simple addition to compare times. */
double red_op(double a, double b) { return a + b; }

/* Make some vector of proper size. We chose {0, 1, 2 ... 10, 0, 1, 2 ... 10, ...} because 
 * this always sums to 4500... so it's easy to check correctness. */
double* make_vector (int size) {

	int i;
	double* out = malloc(size * sizeof(double));
	for (i = 0; i < size; i++)
		out[i] = i % 10;
	return out;
}

/* Find the largest power of two smaller than or equal to input. Taken 
 * (with slight modification) from: 
 * http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2 */
int last_power (int in) {

	int incopy = in;
	in--;
	in |= in >> 1;
	in |= in >> 2;
	in |= in >> 4;
	in |= in >> 8;
	in |= in >> 16;
	in++;
	if (!(incopy == in))
		return in / 2;
	else 
		return in;
}

/* Print a vector given its length. For debugging only */
void print_vector(double* vec, int len) {

	int i;
	for (i = 0; i < len; i++) {
		printf("%f ", vec[i]);
	}
	printf("\n");
}

/* Reduce a vector using addition */
double sum (double* vec, int len) {

	double accu = 0.0;
	int i;

	/* Use openmp to parallelize the forloop. Addition is an operation supported by
	 * the reduction clause in openMP so we can use that. */
	#pragma omp parallel for reduction(+:accu)
	for (i = 0; i < len; i++) {
		accu = accu + vec[i];
	}
	return accu;
}

/* Reduce a vector using an inputted reduction operator */
double naieve_reduce (double (fun)(double, double), double* vec, int len, double neutral) {

	double accu = neutral;
	int i;
	
	/* Use openmp to parallelize the forloop. We cannot use the reduction clause as the 
	 * function might not be supported by it. So we use a critical section */	
	#pragma omp parallel for
	for (i = 0; i < len; i++) {
		#pragma omp critical
		accu = fun(accu, vec[i]);
	}
	return accu;
}

/* Reduce a vector using an inputted reduction operator. Optimized for multithreadi */
double opti_reduce (double (fun)(double, double), double* vec, int len, double neutral) {

	int i;

	/* The process destroys the original memory of vector. Because of this we copy the 
	 * input first */
	double* vec_copy = malloc(len * sizeof(double));
	memcpy(vec_copy, vec, len * sizeof(double));

	/* We need some original locations as well as some pointers we can shift around */
	double* head = vec_copy;
	double* original_vec = vec_copy;
	double* new_vec;
	
	/* We calculate the largest power of two that will fit in len */
	int new_len = last_power(len);
	int new_len_copy;
	int remaining = len;

	double accu = neutral;

	/* We keep splitting out subvectors of vector that have a power of two as their length. 
	 * these subvectors can be folded relatively easily and in parallel. By calling fun on
	 * all subresults we find the final answer. */
	do {
		/* We calculate the size of the vector remaining after removing the first new_len 
		 * elements and copy new_len, as we will modify it during further calculations */
		remaining = remaining - new_len;
		new_len_copy = new_len;

		/* We fold a part of the vector. Since we have guaranteed it is a power of 2. We can
		 * fold the vector in half untill only one element remains */
		while (new_len != 1) {
			new_vec = malloc((new_len / 2) * sizeof(double));
			/* Call fun on all pairs. This can be multithreaded savely without a 
			 * critical section */
			#pragma omp parallel for
			for (i = 0; i < new_len; i += 2) {
				new_vec[i / 2] = fun(vec_copy[i], vec_copy[i + 1]);
			}

			/* Prepare for next step by replacing the working vector by the smaller one
			 * we just calculated */
			memcpy(vec_copy, new_vec, (new_len / 2) * sizeof(double));
			free(new_vec);
			new_len = new_len / 2;
		}

		/* Call func on the fold of this subvector and the fold so far. This must be done 
		 * sequentially */
		accu = fun(accu, vec_copy[0]);
		
		/* Prepare for the next step: increment the start of vector pointer and calculate the
		 * new largest power of two that will fit in the remainder of last step */
		head += new_len_copy;
		vec_copy = head;
		new_len = last_power(remaining);

	} while (remaining != 0);
	free(original_vec);
	return accu;
}

int main (int argc, char** argv) {

	double* vector;
	int thread_num, vector_size, fun, i;
	double result;

	/* Read arguments */
	if (argc == 4) {
		thread_num = atoi(argv[1]);
		vector_size = pow(10.0, atoi(argv[2]));
		fun = atoi(argv[3]);
		vector = make_vector(vector_size);
		omp_set_num_threads(thread_num);
		
		/* start time */
		timer_start();

		/* Fold the vector using either of the functions 10 times. */
		switch (fun) {
			case 0:
				for (i = 0; i < 10; i++) 
					result = sum(vector, vector_size);
				break;
			case 1:
				for (i = 0; i < 10; i++) 
					result = naieve_reduce(&red_op, vector, vector_size, 0);
				break;
			case 2:
				for (i = 0; i < 10; i++)
					result = opti_reduce(&red_op, vector, vector_size, 0);
				break;
		}
		/* End time, print results */
		double time_elapsed = timer_end();
		printf("Folded to: %f (ten times) in %f seconds\n", result, time_elapsed);
		free(vector);
	}
	/* Print usage if wrong number of arguments were given */
	else {
		printf("assignment2_2 <# threads> <log (size of vector)> <function>\n");
	}
	return 0;
} 