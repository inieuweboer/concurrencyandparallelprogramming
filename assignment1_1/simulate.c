/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  simulate.c
 *
 *  The parallel simulation is implemented here.
 * 
 *  The "simulate" function is called from assign1_1.c.
 *  It initializes all worker threads and manages them
 *  to divide the calculations that need to be done.
 */

#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "simulate.h"


/********** Global variables **********/

/* Amount of values done for next_array */
int i_done;
pthread_mutex_t i_done_mtx = PTHREAD_MUTEX_INITIALIZER;

/* Number of idle threads, mutex and conditional */
int nthreads_idle;
pthread_mutex_t nthreads_idle_mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  nthreads_idle_cond  = PTHREAD_COND_INITIALIZER;

/* Predicate that states whether all threads are initialized */
bool threads_are_ready;
pthread_mutex_t threads_are_ready_mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  threads_are_ready_cond  = PTHREAD_COND_INITIALIZER;

/* Number of working threads, mutex and conditional */
int nthreads_working;
pthread_cond_t  nthreads_working_cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t nthreads_working_mtx= PTHREAD_MUTEX_INITIALIZER;

/* Predicate that states whether the thread can continue working */
bool threads_can_continue;
pthread_mutex_t threads_can_continue_mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  threads_can_continue_cond  = PTHREAD_COND_INITIALIZER;


/**********  Functions. **********/

/* Initialize the thread function argument struct */
worker_thread_arg_t* worker_thread_arg_init (int i_max, int t_max,
                                             double** old_array_ptr,
                                             double** current_array_ptr,
                                             double** next_array_ptr) {

    worker_thread_arg_t* worker_thread_arg_ptr = NULL;
    worker_thread_arg_ptr = malloc(sizeof(worker_thread_arg_t));

    if (!worker_thread_arg_ptr) {
        fprintf(stderr, "Error in worker_thread_arg_init; not enough memory");
        exit(EXIT_FAILURE);
    }

    worker_thread_arg_ptr->i_max = i_max;
    worker_thread_arg_ptr->t_max = t_max;
    worker_thread_arg_ptr->old_array_ptr = old_array_ptr;
    worker_thread_arg_ptr->current_array_ptr = current_array_ptr;
    worker_thread_arg_ptr->next_array_ptr = next_array_ptr;

    return worker_thread_arg_ptr;
}

/* Function each new thread starts in. */
void* worker_thread_func (void* void_ptr) {

    int i_next, t;

    worker_thread_arg_t* worker_thread_arg_ptr = (worker_thread_arg_t*)void_ptr;
    int i_max = worker_thread_arg_ptr->i_max;
    int t_max = worker_thread_arg_ptr->t_max;
    double** old_array_ptr     = worker_thread_arg_ptr->old_array_ptr;
    double** current_array_ptr = worker_thread_arg_ptr->current_array_ptr;
    double** next_array_ptr    = worker_thread_arg_ptr->next_array_ptr;

    /* Amount of waves to generate, not including the first two */
    for (t = 0; t < t_max - 2; ++t) {
        /* Signals to the main thread that it is going idle. */
        pthread_mutex_lock(&nthreads_idle_mtx);
        ++nthreads_idle;
        pthread_cond_signal(&nthreads_idle_cond);
        pthread_mutex_unlock(&nthreads_idle_mtx);

        /* Waits until the main thread gives the signal to start working. */
        pthread_mutex_lock(&threads_are_ready_mtx);
        while (!threads_are_ready) {
            pthread_cond_wait(&threads_are_ready_cond, &threads_are_ready_mtx);
        }
        pthread_mutex_unlock(&threads_are_ready_mtx);

        /* When the signal is given, gets next point to calculate,
         * calculates and stores it */
        while ((i_next = get_next_point(i_max)) != DONE)
            (*next_array_ptr)[i_next] =
                calc_point(i_next, i_max, *old_array_ptr, *current_array_ptr);

        /* When there are no more points to calculate, it marks itself
         * as finished and signals this to the main thread */
        pthread_mutex_lock(&nthreads_working_mtx);
        --nthreads_working;
        pthread_cond_signal(&nthreads_working_cond);
        pthread_mutex_unlock(&nthreads_working_mtx);

        /* Waits for the signal to start the next iteration */
        pthread_mutex_lock(&threads_can_continue_mtx);
        while (!threads_can_continue) {
            pthread_cond_wait(&threads_can_continue_cond,
                              &threads_can_continue_mtx);
        }
        pthread_mutex_unlock(&threads_can_continue_mtx);
    }

    pthread_exit(NULL);
}

/* Gives the next amplitude point to calculate */
int get_next_point (int i_max) {
    int i_next;

    /* Lock the mutex, get the index and increment the counter, and unlock */
    pthread_mutex_lock(&i_done_mtx);
    i_next = i_done++;
    pthread_mutex_unlock(&i_done_mtx);

    return (i_next < i_max ? i_next : DONE);     /* Return DONE if done  */
}


/* Calculates the given wave function at position i and time t+1.
 * For the sake of readable code, old_array == old && current_array == curr */
double calc_point (int i, int i_max, double* old, double* curr) {
    /* Zero at the edges by definition */
    return (i == 0 || i == i_max ? 0.0 :
            2*curr[i] - old[i] + C*(curr[i-1] - 2*curr[i] + curr[i+1]) );
}


/*
 * Executes the entire simulation.
 *
 * i_max: how many data points are on a single wave
 * t_max: how many iterations the simulation should run
 * nthreads: how many threads to use (excluding the main threads)
 * old_array: array of size i_max filled with data for t-1
 * current_array: array of size i_max filled with data for t
 * next_array: array of size i_max. You should fill this with t+1
 */
double* simulate (const int i_max, const int t_max, const int nthreads,
        double* old_array, double* current_array, double* next_array) {

    int check, j, t;

    pthread_t threads[nthreads];
    worker_thread_arg_t* worker_thread_arg_ptr =
        worker_thread_arg_init(i_max, t_max,
                         &old_array, &current_array, &next_array);

    /* Set initial values for global variables, before threads are created */
    nthreads_idle = 0;
    threads_are_ready = false;

    /* Create and run threads */
    for (j = 0; j < nthreads; ++j) {
        check = pthread_create(&threads[j], NULL, &worker_thread_func,
                                (void*)worker_thread_arg_ptr);
        if (check) {
            fprintf(stderr, "Error in simulate;"
                            "return code from pthread_create() is %d\n", check);
            exit(EXIT_FAILURE);
        }
    }

    /* For each timestep, manage the worker threads and swap the arrays. */
    for (t = 0; t < t_max - 2; ++t)
        manage_worker_threads(nthreads, worker_thread_arg_ptr);

    /* Cleanup */
    for (j = 0; j < nthreads ; ++j)
        pthread_join(threads[j], NULL);

    free(worker_thread_arg_ptr);

    /* Return a pointer to the array with the final results. */
    return current_array;
}


/* This function manages the worker threads (it sleeps for the most part)
 * It also needs the worker argument struct to make sure
 * it rotates the same arrays */
void manage_worker_threads (int nthreads,
                            worker_thread_arg_t* worker_thread_arg_ptr) {
    /* Wait until all threads are ready. */
    pthread_mutex_lock(&nthreads_idle_mtx);
    while (nthreads_idle != nthreads) {
        pthread_cond_wait(&nthreads_idle_cond, &nthreads_idle_mtx);
    }
    pthread_mutex_unlock(&nthreads_idle_mtx);

    /* All threads are waiting so mutexes don't have to be set;
     * first mutex prevents them from starting with the next iteration. */
    threads_can_continue = false;
    nthreads_working = nthreads;
    i_done = 0; /* No calculations are done yet */

    /* Signal the threads can start */
    pthread_mutex_lock(&threads_are_ready_mtx);
    threads_are_ready = true;
    pthread_cond_broadcast(&threads_are_ready_cond);
    pthread_mutex_unlock(&threads_are_ready_mtx);

    /* Go idle and wait for the worker threads to finish */
    pthread_mutex_lock(&nthreads_working_mtx);
    while (nthreads_working) {
        pthread_cond_wait(&nthreads_working_cond, &nthreads_working_mtx);
    }
    pthread_mutex_unlock(&nthreads_working_mtx);

    /* When the worker threads are done, rotate the arrays. */
    swap_3_double_ptrs(worker_thread_arg_ptr->old_array_ptr,
                       worker_thread_arg_ptr->current_array_ptr,
                       worker_thread_arg_ptr->next_array_ptr);
    /* Make sure the worker threads go idle in the next iteration */
    threads_are_ready = false;
    nthreads_idle = 0;

    /* Allow them to finish the current iteration */
    pthread_mutex_lock(&threads_can_continue_mtx);
    threads_can_continue = true;
    pthread_cond_broadcast(&threads_can_continue_cond);
    pthread_mutex_unlock(&threads_can_continue_mtx);
}

/* Swap three pointers; therefore function needs pointers to these pointers */
void swap_3_double_ptrs (double** first_pptr,
                         double** second_pptr,
                         double** third_pptr) {
    double* temp_ptr;

    temp_ptr     = *first_pptr;
    *first_pptr  = *second_pptr;
    *second_pptr = *third_pptr;
    *third_pptr  =  temp_ptr;
}
