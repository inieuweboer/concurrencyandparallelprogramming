/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  simulate.h
 *
 *  Macros, struct for argument, and function declarations
 */

#pragma once
#define C 0.15  /* Spatial impact */
#define DONE -1 /* Used for each loop across a waveform */


typedef struct worker_thread_arg_t worker_thread_arg_t;
struct worker_thread_arg_t {

    int i_max;
    int t_max;
    double** old_array_ptr;
    double** current_array_ptr;
    double** next_array_ptr;
};


worker_thread_arg_t* worker_thread_arg_init (int, int,
        double**, double**, double**);
void* thread_calc_waveform (void*);
int get_next_point (int);
double calc_point (int, int, double*, double*);

double* simulate (const int i_max, const int t_max, const int num_cpus,
        double* old_array, double* current_array, double* next_array);
void manage_worker_threads (int, worker_thread_arg_t*);
void swap_3_double_ptrs (double**, double**, double**);
