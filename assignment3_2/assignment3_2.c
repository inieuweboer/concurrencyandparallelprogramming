/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  assignment3_2.c
 *
 *  Implementation of a broadcast function and a small test for this function.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

/* Our main contains an example of a broadcast. The argument given on execution will be used as
 * root node. A buffer of 4 elements (2, 3, 4, 5) will be broadcasted. Each process will report
 * the sum of the numbers it received as a check. */
int main (int argc, char* argv[]) {

	int rank, size, count, accu, root, i;

	/* Initialize MPI and report */
	MPI_Init(&argc, &argv);		
  	MPI_Comm_rank(MPI_COMM_WORLD, &rank);     
  	MPI_Comm_size(MPI_COMM_WORLD, &size);
  	printf( "Process %d of %d is alive!\n", rank, size );

	/* Get root process from arguments */
	if (argc == 2)
		root = mod(atoi(argv[1]), size);
	else {
		printf("No argument, proces %d shutting down...\n", rank);
		return 1;
	}

	/* Set some count and an accumulator to check results with later and initialize the buffer */
  	count = 4;
  	accu  = 0;
  	void* buffer = calloc(count, sizeof(int));

  	/* If this is the root, make another buffer with some numbers and broadcast it */
  	if (rank == root) {
		void* my_buffer = calloc(count, sizeof(int));
		for (i = 0; i < count; i++)
			((int*)my_buffer)[i] = i + 2;
  		MYMPI_Bcast (my_buffer, count, MPI_INT, root, MPI_COMM_WORLD);
  	}
  	/* Else just use the regular buffer */
  	else 
  		MYMPI_Bcast (buffer, count, MPI_INT, root, MPI_COMM_WORLD);
  	
  	/* Print the sum of received data to check if everything went OK. Root should report 0, 
  	 * other processes should report 14. */
  	for (i = 0; i < count; i++)
 		accu += ((int*)buffer)[i];
 	printf("%d reports checksum %d\n", rank, accu);

  	MPI_Finalize();
  	return 0;
}

/* Implements the broadcast function specified in the assignment */
int MYMPI_Bcast (void* buffer, int count, MPI_Datatype datatype, int root,
			     MPI_Comm communicator) {

	MPI_Status status;

	/* Get rank and size and check if this is the root process */
	int rank, size, is_root, i;
	MPI_Comm_rank(communicator, &rank);
	MPI_Comm_size(communicator, &size);
	is_root = (root == rank);

	/* If only one process exists, we don't have to broadcast anything */
	if (size == 1)
		return 0;

	/* If we are not root, we should wait untill we get a message */
	if (!is_root) {
		MPI_Recv(buffer, count, datatype, MPI_ANY_SOURCE, 10, communicator, &status);

		/* Identify who sent us a message */
		int sender = status.MPI_SOURCE;

		/* If the message came from above, send it down. If it came from below, send it up */
		if (sender == mod(rank + 1, size))
			MPI_Send(buffer, count, datatype, mod(rank - 1, size), 10, communicator);
		else
			MPI_Send(buffer, count, datatype, mod(rank + 1, size), 10, communicator);
	}
	/* If we are root, we should send a message to both our neighbours */
	else {
		MPI_Send(buffer, count, datatype, mod(root + 1, size), 10, communicator);
		MPI_Send(buffer, count, datatype, mod(root - 1, size), 10, communicator);
	}

	return 1;
}

/* Modified version of the '%' operator that instead returns the smallest positive 
 * representative of a mod b. Only works if abs(a) < b */
int mod(int a, int b) { return (a >= 0) ? (a % b) : ((a % b) + b); }


