/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  simulate.h
 *
 *  Macros, struct for argument, and function declarations
 */

#pragma once
#define C 0.15  /* Spatial impact */

double* simulate (const int, const int, const int, double*, double*, double*);

