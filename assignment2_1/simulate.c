/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  simulate.c
 *
 *  The parallel simulation is implemented here.
 *
 *  The "simulate" function is called from assign1_1.c.
 *  It initializes all worker threads and manages them
 *  to divide the calculations that need to be done.
 */

#include <stdio.h>
#include <stdlib.h>

#include "omp.h"

#include "simulate.h"


inline double calc_point (int i, double* old, double* curr) {
    return 2*curr[i] - old[i] + C*(curr[i-1] - 2*curr[i] + curr[i+1]);
}

/*
 * Executes the entire simulation.
 *
 * i_max: how many data points are on a single wave
 * t_max: how many iterations the simulation should run
 * nthreads: how many threads to use (excluding the main threads)
 * old_array: array of size i_max filled with data for t-1
 * current_array: array of size i_max filled with data for t
 * next_array: array of size i_max. You should fill this with t+1
 */
double* simulate (const int i_max, const int t_max, const int nthreads,
        double* old_array, double* current_array, double* next_array) {

    int t, i;
    double* temp;

    /* Set num of threads */
    omp_set_num_threads(nthreads);

    /* This code is parallelized.
     * Not including the first two generations. */
    #pragma omp parallel for private(t, i)
    for (t = 0; t < t_max - 2; ++t) {

        /* The first and last point of a generation are known already,
         * and are memsetted to zero in assign2_1.c */
        for (i = 1; i < i_max - 1; ++i)
            next_array[i] = calc_point(i, old_array, next_array);

        /* Swap the arrays */
        #pragma omp critical
        {
            temp = old_array;
            old_array = current_array;
            current_array = next_array;
            next_array = temp;
        }
    }

    /* Return a pointer to the array with the final results. */
    return current_array;
}
