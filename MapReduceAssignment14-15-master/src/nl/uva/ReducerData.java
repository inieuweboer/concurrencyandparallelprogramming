package nl.uva;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.DoubleWritable;

/*  
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  Implementation of a custom writable class that stores two writable integers and two
 * 	writable doubles. Used to store the number of occurences, the number of occurences in
 *  English, the mean sentiment value and the standard deviation of the sentiment value of
 *  a key. Each method implemented here to comply with the Writable interface simply 
 *  uses the methods in its fields, which are already writable Objects.
 */

public class ReducerData implements Writable {

	private IntWritable occurences = new IntWritable(0);
	private IntWritable engOccurences = new IntWritable(0);
	private DoubleWritable mean = new DoubleWritable(0);
	private DoubleWritable sd = new DoubleWritable(0);

	public ReducerData (int occurences, int engOccurences, double mean, double sd) {

		this.occurences = new IntWritable(occurences);
		this.engOccurences = new IntWritable(engOccurences);
		this.mean = new DoubleWritable(mean);
		this.sd = new DoubleWritable(sd);
	}

	public void set (int occurences, int engOccurences, double mean, double sd) {

		this.occurences.set(occurences);
		this.engOccurences.set(engOccurences);
		this.mean.set(mean);
		this.sd.set(sd);
	}

	@Override
	public void write (DataOutput out) throws IOException {

		occurences.write(out);
		engOccurences.write(out);
		mean.write(out);
		sd.write(out);
	}
	@Override
	public void readFields(DataInput dataInput) throws IOException {
		
		occurences.readFields(dataInput);
		engOccurences.readFields(dataInput);
		mean.readFields(dataInput);
		sd.readFields(dataInput);
	}

	@Override
	public String toString () {

		return "" + this.occurences + " | " + this.engOccurences +
			   " | " +  this.mean + " | " + this.sd;
	}
}
