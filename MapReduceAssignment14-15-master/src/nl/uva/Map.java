package nl.uva;

import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import me.champeau.ld.UberLanguageDetector;

/* Added */
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import java.util.ArrayList;

/* Sentiment analysis */
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

import java.lang.Math;

/**
 * Word count mapper.
 *
 * @author S. Koulouzis
 */

/*  Edited by:
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  Added:
 *      - Method to find sentiments using stanford libraries (from PDF).
 *      - Override of configure method that fetches filepaths for .ser.gz's needed.
 *  Changed:
 *      - Map method to perform the required analysis.
 */

public class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {

    Log log = LogFactory.getLog(Map.class);
    private Text word = new Text();

    private String valueStr;
    private String wordStr;

    private Path[] paths;
    Path sentimentModelPath;
    Path parseModelPath;

    /* Even though there might not even be any standards implemented for
     * hashtags (see: https://shkspr.mobi/blog/2010/02/hashtag-standards/)
     * bits of this regex are taken from different sources
     * stackoverflow.com/questions/4984806/java-regex-extract-hashtags-from-string
     * stackoverflow.com/questions/1563844/best-hashtag-regex
     */
    private static final Pattern hashtagPattern =
        Pattern.compile("(?:^|\\s|[\\p{Punct}&&[^/]])(#[\\p{L}0-9_]*[\\p{L}_]+[\\p{L}0-9_]*)");
    private Matcher hashtagMatcher;

    static enum Counters {

        INPUT_LINES
    }

    /* This function is needed to determine file paths, taken from assignment pdf */
    @Override
    public void configure(JobConf conf) {

        try {
            Job job = Job.getInstance(conf);
            paths = DistributedCache.getLocalCacheFiles(conf);
            parseModelPath = paths[0];
            sentimentModelPath = paths[1];
            for (Path p : paths)
                System.out.println("" + p);
        }
        catch (IOException ex) {
            Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    /* Finds the sentiment of a sentence, taken from assignment pdf */
    private int findSentiment(String text) {

        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
        props.put("parse.model", parseModelPath.toString());
        props.put("sentiment.model", sentimentModelPath.toString());
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

        int mainSentiment = 0;
        if (text != null && text.length() > 0) {
            int longest = 0;
            Annotation annotation = pipeline.process(text);
            for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
                Tree tree = sentence.get(SentimentCoreAnnotations.AnnotatedTree.class);
            int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
            String partText = sentence.toString();
                if (partText.length() > longest) {
                    mainSentiment = sentiment;
                    longest = partText.length();
                }
            }
        }

        //This method is very demanding so try so save some memory
        pipeline = null;
        System.gc();
        return mainSentiment;
    }


    /* The map function which is called by each node */
    @Override
    public void map (LongWritable key, Text value, OutputCollector<Text, IntWritable> oc,
                     Reporter rprtr) throws IOException {

        /* Check if we're at the tweet line */
        if (value.charAt(0) == 'T' || value.charAt(0) == 'U' || value.equals(""))
            return;

        int count = 0;

        /* Cast sentence to String because we need regex, initialize matcher and
         * list to loop across hashtags and temporarily store them */
        valueStr = value.toString();
        hashtagMatcher = hashtagPattern.matcher(valueStr);
        ArrayList<String> hashtags = new ArrayList<String>();

        /* A sentiment of -1 means there is none. */
        IntWritable sentiment = new IntWritable(-1);
        boolean sentimentCalculated = false;

        while (hashtagMatcher.find()) {

            /* As a convention count all hashtags in lowercase, as
             * they are case insensitive. */
            wordStr = hashtagMatcher.group(1).toLowerCase();
            word = new Text(wordStr);

            /* Check if we havent already encountered this hashtag in this tweet and 
             * if we have, skip it */
            if (hashtags.contains(wordStr))
                continue;
            else
                hashtags.add(wordStr);

            /* Check if the tweet is in english, if not just count hashtags.
             * The sentimentdcalculated boolean makes sure we only call the heavy
             * detectLang and heavier findSentiment methods once per tweet */
            if (!sentimentCalculated) {
                sentimentCalculated = true;

                String lang = UberLanguageDetector.getInstance().detectLang(valueStr);
                if (lang != null && lang.equals("en")) {
                    sentiment.set(findSentiment(valueStr));
                }
            }

            oc.collect(new Text(wordStr), sentiment);

            /* Information for the framework logger */
            rprtr.incrCounter(Counters.INPUT_LINES, 1);
            count++;
            if ((count % 100) == 0) {
                rprtr.setStatus("Finished processing " + count + " records");
            }
        }
    }
}
