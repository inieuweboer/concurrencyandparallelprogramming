package nl.uva;

import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

/**
 *
 *
 * @author S. Koulouzis
 */

/*  Edited by:
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  Changed reduce method to produce the required data (occurences, mean, standard deviation)
 *  per key.  
 */

public class Reduce extends MapReduceBase implements Reducer<Text, IntWritable, Text, ReducerData> {

    static enum Counters {

        OUTPUT_LINES
    }

    /* Reduction method. Uses a custom writable class for its output collector that contains 
     * two integers and two doubles. This class is implemented in ReducerData.java. For each
     * key this method will calculate the amount of occurences total, the amount of occurences
     * in english sentences, the mean sentiment and the standard deviation of the sentiment. */
    @Override
    public void reduce(Text key, Iterator<IntWritable> itrtr, OutputCollector<Text, ReducerData> output, Reporter rprtr) throws IOException {

        int sum, count, current;
        double average, sd;

        /* countEnglish keeps track of the amount of english sentences the key was found in,
         * hasSentiment keeps track of whether it was found in at least one english sentence */
        int countEnglish;
        boolean hasSentiment = false;

        average = sd = sum = count = countEnglish = 0;
        current = -1;

        /* Loop through all the values associated with this key. */
        while (itrtr.hasNext()) {
            current = itrtr.next().get();

            /* Value -1 indicates that the sentence in which this key was 
             * found was not in English */
            if (current != -1) {
                hasSentiment = true;
                countEnglish ++;

                /* Keep track of both sum and quadratic sum */
                sum += current;
                sd += current * current;
            }

            count++;
            if ((count % 100) == 0) {
                rprtr.setStatus("Finished processing " + count + " records ");
            }
        }

        /* Calculate average and standard deviation if at least one sentiment was calculated */
        if (hasSentiment) {
            average = ((double) sum) / countEnglish;
            /* We use a little trick here, the standard deviation squared (1/n sum (x - mean) ^ 2) 
             * can be rewritten to (1/n * sum (x^2) - mean^2). Making the calculation possible
             * in one pass */
            sd = Math.sqrt((sd / countEnglish) - average * average);

            /* Finally collect a ReducerData object contructed from the found values */
            output.collect(key, new ReducerData(count, countEnglish, average, sd));
        }
        /* Otherwise use -1 for the values we couldn't find and collect a ReducerData Object. */
        else
            output.collect(key, new ReducerData(count, 0, -1, -1));
        rprtr.incrCounter(Counters.OUTPUT_LINES, 1);
    }
}
