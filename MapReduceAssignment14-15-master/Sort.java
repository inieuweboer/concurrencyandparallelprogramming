/* Sort the output from MapReduce */

import java.io.*;
import java.util.*;

/*
public class Sort {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader("out/part-00000"));
        Map<String, String> map=new TreeMap<String, String>();
        String line="";
        while((line=reader.readLine())!=null){
            map.put(getField(line),line);
        }
        reader.close();
        FileWriter writer = new FileWriter("out/part-00000-sorted");
        for(String val : map.values()){
            writer.write(val);
            writer.write('\n');
        }
        writer.close();
    }

    private static String getField(String line) {
        return line.split("\\s+")[1];//extract value you want to sort on
    }
}
*/


public class Sort {

    public static void main(String[] args) {

        try {
            sortObserve();
        }
        catch(Exception e) {
            System.out.println("shit gaat fout");
        }

    }

    public static void sortObserve() throws IOException {

        Comparator string_comp = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                            // provide your comparison logic here
                int count1 = Integer.parseInt(o1.split("\\s+")[1]);
                int count2 = Integer.parseInt(o2.split("\\s+")[1]);

                if (count1 > count2)
                    return 1;
                else if (count1 < count2)
                    return -1;

                return 0;
            }
        };


        File fin = new File("./out/part-00000");
        File fout = new File("./out/part-00000-sorted");

        FileInputStream fis = new FileInputStream(fin);
        FileOutputStream fos = new FileOutputStream(fout);

        BufferedReader in = new BufferedReader(new InputStreamReader(fis));
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos));

        String aLine;
        ArrayList<String> al = new ArrayList<String> ();

        int i = 0;
        while ((aLine = in.readLine()) != null) {
            //get the lines you want, here I don't want something starting with - or empty

            //if (!aLine.trim().startsWith("-") && aLine.trim().length() > 0) {
            if (aLine.trim().length() > 0) {
                al.add(aLine);
                i++;
            }
        }

        Collections.sort(al, string_comp);

        //output sorted content to a file
        for (String s : al) {
            //System.out.println(s);
            out.write(s);
            out.newLine();
        }

        in.close();
        out.close();
    }

}
