#include "queue.h"
#include <pthread.h>

typedef struct thread_info_t thread_info_t;

struct thread_info_t { 

	queue_t* queue;
	pthread_mutex_t* new_mutex;
	int number;
};

thread_info_t* thread_info_init(queue_t* q, pthread_mutex_t* new_mutex, int n);
