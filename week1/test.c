#include "thread_info.h"
#include "test.h"

void* subthread (void* info) {

	/* Process thread info */
	thread_info_t* infoc = (thread_info_t*) info;
	queue_t* queue_in = (*infoc).queue;	
	pthread_mutex_t* mutex_left = (*infoc).new_mutex;

	int number = (*infoc).number; // What this thread will divide by
	int current = 0;			  // Number to be tested

	/* Keep track of whether this is last thread in chain */
	int isLast = 1;

	/* Make stuff for new thread */
	queue_t* queue_out = queue_init();
	pthread_mutex_t mutex_right;
	pthread_mutex_init(&mutex_right, NULL);
	pthread_t next_thread;
	thread_info_t* next_info;
	
	while (1) {
		/* Check if inputqueue has elements */
		pthread_mutex_lock(mutex_left); H VY:0
		if ((*queue_in).size > 0) {
			/* Try grabbing an element */
			current = (int) dequeue(queue_in);
			pthread_mutex_unlock(mutex_left);

			/* If current is not divisable by number we need to do something */
			if ((current % number)) {
				/* If this thread is last in chain, make a new thread, and print current */
				if (isLast) {
					printf("%d\n", current);
					next_info = thread_info_init(queue_out, &mutex_right, current);
					pthread_create(&next_thread, NULL, &subthread, next_info);
					isLast = 0;
				}
				/* Otherwise pass it through to the next thread */
				else {
					pthread_mutex_lock(&mutex_right);
					enqueue(queue_out, (int*) current);
					pthread_mutex_unlock(&mutex_right);
				} 
			}
		} else {
			pthread_mutex_unlock(mutex_left);
		}
	}
}

void* produce_numbers (void* q) {

	/* Create first pipeline stage */
	pthread_mutex_t mutex_right;
	pthread_mutex_init(&mutex_right, NULL);

	pthread_t next_thread;
	queue_t* queue_out = queue_init();
	thread_info_t* next_info = thread_info_init(queue_out, &mutex_right, 2);

	pthread_create(&next_thread, NULL, &subthread, (void*) next_info);

	/* Keep enqueueing numbers starting at three */
	int counter = 3;
	while (1) {
		if (enqueue(counter) != -1)
			counter ++;
	}
}

int main() {
	
	pthread_t thread1;

	pthread_create(&thread1, NULL, &produce_numbers, NULL);
	pthread_join(thread1, NULL);

	return 0;
}