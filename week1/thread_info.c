#include "test.h"
#include "thread_info.h"

thread_info_t* thread_info_init(queue_t* q, pthread_mutex_t* new_mutex, int n) {

	/* Set memory */
	thread_info_t* out = malloc(sizeof(thread_info_t));
	assert(out);

	/* Set all fields */
	out->number = n;
	out->queue = q;
	out->new_mutex = new_mutex;

	return out;
}
