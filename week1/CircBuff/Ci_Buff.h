/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  Ci_Buff.h:
 *
 *  Header for Ci_Buff.c and declaration of the ci_buff_t struct
 *  representing a circular buffer.
 */

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct ci_buff_t ci_buff_t;

struct ci_buff_t {

	int head;
	int tail;
	int size;
	int items;
	int* buffer;
	pthread_cond_t* is_full;
	pthread_cond_t* is_empty;
	pthread_mutex_t* lock;
};

ci_buff_t* 	ci_buff_init(int size);
int 		enqueue(ci_buff_t* cb, int in);
int 		dequeue(ci_buff_t* cb);
