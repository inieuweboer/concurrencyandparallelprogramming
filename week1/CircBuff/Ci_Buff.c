/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  Ci_Buff.c:
 *
 *  Implementation of a circular buffer of positive integers.
 *  Uses an empty element to deal with head / tail conflict at
 *  empty / full buffers. Contains a mutex to lock the
 *  queue aswell as conditionals to indicate the buffer is full / empty. 
 */

#include "Ci_Buff.h"

ci_buff_t* ci_buff_init (int size) {

	ci_buff_t* ci_buff 	= calloc(1, sizeof(ci_buff_t));
	(*ci_buff).buffer 	= calloc(size + 1, sizeof(int));
	(*ci_buff).head 	= 0;
	(*ci_buff).tail 	= 0;
	(*ci_buff).size 	= size + 1; // +1 for empty element 
	(*ci_buff).lock 	= calloc(1, sizeof(pthread_mutex_t));
	(*ci_buff).is_full  = calloc(1, sizeof(pthread_cond_t));
	(*ci_buff).is_empty = calloc(1, sizeof(pthread_cond_t));
	pthread_mutex_init	((*ci_buff).lock, 	  NULL);
	pthread_cond_init 	((*ci_buff).is_full,  NULL);
	pthread_cond_init 	((*ci_buff).is_empty, NULL);

	return ci_buff;
}

/* Check if the buffer is full by comparing head and tail pointer */
int ci_buff_is_full (ci_buff_t* cb) {

	int out = ((*cb).tail + 1) % (*cb).size == (*cb).head;
	return out;
}

/* Enqueue an item. If the buffer is full return -1 */
int enqueue (ci_buff_t* cb, int in) {
	
	if (ci_buff_is_full(cb))
		return -1;
	else {		
		(*cb).buffer[(*cb).tail] = in;
		(*cb).tail = ((*cb).tail + 1) % (*cb).size;
		return 1;
	}
}

/* Return the head of the buffer and adjust the head pointer.
 * If the buffer is empty return -1 */
int	dequeue (ci_buff_t* cb) {

	if ((*cb).tail == (*cb).head) {
		return -1;
	}
	else {
		int out = (*cb).buffer[(*cb).head];
		(*cb).head = ((*cb).head + 1) % (*cb).size;
		return out;
	}
}
