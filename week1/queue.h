typedef struct queue_t queue_t;
typedef struct queue_item_t queue_item_t;

struct queue_t {

	queue_item_t* head;
	queue_item_t* tail;
	int size;
};

struct queue_item_t {

	int* data;
	queue_item_t* next_item;
};

queue_t* queue_init();
void 	 enqueue(queue_t* q, int* data);
int* 	 dequeue(queue_t* q);
void 	 delete(queue_t* q, void (*f)(int*));

