/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 * Sieve.h:
 *
 * Header for Sieve.c
 */

void* pipeline_stage (void* left_buff_void);
void* produce_numbers ();
int main ();
