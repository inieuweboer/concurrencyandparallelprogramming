/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  Sieve.c:
 *
 *  Implementation of a sieve of Erotosthenes. Uses a pipeline of threads that
 *  discard muliples of a certain number to filter out non-primes from a stream 
 *  of all natural numbers. Any number that makes it to the end of the pipeline 
 *  will be  printed. A new stage of the pipeline is then appended that filters 
 *  multiples of this number.
 */

#include <stddef.h> 
#include <pthread.h>

#include "Ci_Buff.h"
#include "timer.h"

/* Global variables */
int primes_calculated = 0;
int generated		  = 0;

/* Arguments */
int buffer_size;
int max_primes;
int verbose;

/* Void used by all pipeline stages except for the first one. Receives a queue
from its creator which it uses as left queue. Prints original head of this queue
(which always exists) and uses it as divider. Continually tries to divide numbers
fed from creator by this divider. If a number is divisable it is discarded. 
If it is not divisable it is passed to the next stage. If no next stage exists, a
new thread is created.*/
void* pipeline_stage (void* left_buff_void) {

	/* Process starting information */
	ci_buff_t* left_buffer = (ci_buff_t*) left_buff_void;
	int divider;
	while((divider = dequeue(left_buffer)) == -1);
	int current = 0;
	int is_last = 1;

	/* If verbose is true print progress */
	primes_calculated ++;
	if (verbose)
		printf("Curent prime: %d \t(%d / %d) \r", divider, primes_calculated + 1, max_primes + 1);
	
	/* Stop if enough primes have been calculated */
	if (primes_calculated >= max_primes) {
		printf("\nDone in %f seconds. Final prime was: %d\n", timer_end(), divider);
		printf("Generated %d numbers total.\n", generated);
		exit(0);
	}

	ci_buff_t* left_in_buffer = ci_buff_init(buffer_size);
	
	/* Create a new thread (which will be initialized later) and a right buffer */
	pthread_t next_thread;
	ci_buff_t* right_buffer = ci_buff_init(buffer_size);

	/* Make some shortcuts for locks / conditions*/
	pthread_mutex_t* right_lock 	= (*right_buffer).lock;
	pthread_mutex_t* left_lock 		= (*left_buffer).lock;
	pthread_cond_t*	 right_full 	= (*right_buffer).is_full;
	pthread_cond_t*	 right_empty 	= (*right_buffer).is_empty;
	pthread_cond_t*	 left_full 		= (*left_buffer).is_full;
	pthread_cond_t*	 left_empty 	= (*left_buffer).is_empty;

	while (1) {
		/* Lock the left queue and try to fetch input */
		pthread_mutex_lock(left_lock);

		/* If no input exists, suspend thread until it does */
		while ((current = dequeue(left_buffer)) == -1)
			pthread_cond_wait(left_empty, left_lock);

		/* Signal that the left queue is now empty and unlock it*/
		if (ci_buff_state(left_buffer) == -1)
			pthread_cond_signal(left_full);
		pthread_mutex_unlock(left_lock);

		/* Check if we need to pass the input through or discard it */
		if (current % divider) {
			/* Lock the right queue and try to push the input to it */
			pthread_mutex_lock(right_lock);

			/* If the right queue is full, suspend until it isn't */
			while(enqueue(right_buffer, current) == -1)
				pthread_cond_wait(right_full, right_lock);

			/* If no next pipeline stage exists, create a new one */
			if (is_last) {
				pthread_create(&next_thread, NULL, &pipeline_stage, right_buffer);
				is_last = 0;
			}
			/* Signal that the right queue is now full and unlock it */
			if (ci_buff_state(right_buffer) == 1)
				pthread_cond_signal(right_empty);
			pthread_mutex_unlock(right_lock);
		} 
	}
}

/* Void used only by the first pipeline stage. Creates the second stage and 
 * continuingly feeds numbers to it */
void* produce_numbers () {

	/* Create the second pipeline stage */
	pthread_t next_thread;
	ci_buff_t* right_buffer = ci_buff_init(buffer_size);
	pthread_create(&next_thread, NULL, &pipeline_stage, right_buffer);

	/* Make some shortcuts for locks / conditions */
	pthread_mutex_t* right_lock 	= (*right_buffer).lock;
	pthread_cond_t*	 right_full 	= (*right_buffer).is_full;
	pthread_cond_t*	 right_empty 	= (*right_buffer).is_empty;

	/* Keep enqueueing numbers starting at three, only enqueue odd ones */
	int counter = 1;
	while (1) {
		pthread_mutex_lock(right_lock);

		while (ci_buff_is_full(right_buffer))
			pthread_cond_wait(right_full, right_lock);
		
		enqueue(right_buffer, (counter += 2));
		
		if (ci_buff_state(right_buffer) == 1)
			pthread_cond_signal(right_empty);
		
		pthread_mutex_unlock(right_lock);
		generated ++;
	}
}

int main (int argc, char* argv[]) {

	/* Fetch arguments if given*/
	if (argc == 4) {
		buffer_size = atoi(argv[1]);
		max_primes = atoi(argv[2]) - 1;
		verbose = atoi(argv[3]);
	}
	/* Else use standard values */
	else {
		buffer_size = 10;
		max_primes = 999;
		verbose = 1;
	}

	/* Create the first thread and wait for it to finish (which will never happen) */
	pthread_t core_thread;

	timer_start();
	pthread_create(&core_thread, NULL, &produce_numbers, NULL);
	pthread_join(core_thread, NULL);

	return 0;
}
