/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  simulate.cu
 *
 *  The parallel simulation is implemented here.
 *
 *  The "wave_calc_cuda" function is called from wavesim.cu
 *  It sets up the CUDA pointers and memory and then launches the kernel
 *  one time for every generation
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "simulate.h"

/* Utility function, used to do error checking.
   Use this function like this:
   checkCudaCall(cudaMalloc((void **) &deviceRGB, imgS * sizeof(color_t)));

   And to check the result of a kernel invocation:
   checkCudaCall(cudaGetLastError());
*/
static void checkCudaCall (cudaError_t result) {
    if (result != cudaSuccess) {
        printf("Cuda error in simulate.cu: %s\n", cudaGetErrorString(result));
        exit(1);
    }
}


/* Calculates the given wave function at position i and time t+1.
 * old_array == old && current_array == curr */
__device__ double calc_point (int i, double* old, double* curr) {
    return 2*curr[i] - old[i] + C*(curr[i-1] - 2*curr[i] + curr[i+1]);
}


/* Kernel function that simply calculates and stores
 * directly from and to global memory. */
__global__ void wave_calc_kernel (double* device_old, double* device_curr,
            double* device_next) {

    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int i_max = gridDim.x * blockDim.x;

    /* Arrays were memset to zero and the edges of the
     * entire interval are by definition zero */
    if (index != 0 && index != i_max - 1)
        device_next[index] = calc_point(index, device_old, device_curr);
}


/* Kernel function that uses shared memory for the current generation, as
 * almost each entry is used three times */
__global__ void wave_calc_shared_kernel (double* device_old, double* device_curr,
            double* device_next) {

    int block_offset = blockIdx.x * blockDim.x;
    int index = block_offset + threadIdx.x;
    int i_max = gridDim.x * blockDim.x;

    /* Shared memory is only useful for device_curr, since for each kernel
     * launch, every entry of device_old is only accessed once */
    extern __shared__ double buf[];

    /* Since the shared memory is divided as
     * |halo|entries from curr gen for this block|halo|,
     * we want a pointer to the start of the entries from the current gen. */
    double* block_old = device_old + block_offset;
    double* shared_curr = buf + 1;

    /* Each thread fills one entry, except the first and last thread which
     * will also fill the halo cells (if existent) */
    shared_curr[threadIdx.x] = device_curr[index];

    if (threadIdx.x == 0 && index != 0)
        shared_curr[-1] = device_curr[blockIdx.x * blockDim.x - 1];
    else if (threadIdx.x == blockDim.x - 1 && index != i_max - 1)
        shared_curr[blockDim.x] = device_curr[(blockIdx.x + 1) * blockDim.x];

    __syncthreads();

    /* Arrays were memset to zero and the edges of the
     * entire interval are by definition zero */
    if (index != 0 && index != i_max - 1)
        device_next[index] = calc_point(threadIdx.x, block_old, shared_curr);
}


float wave_calc_cuda (int i_max, int t_max, int threads_per_block,
            double* old, double* curr, double* next) {
    int nblocks = i_max/threads_per_block;

    /* Allocate global memory on the GPU */
    double* device_old = NULL;
    checkCudaCall(cudaMalloc((void **) &device_old, i_max * sizeof(double)));
    if (device_old == NULL) {
        printf("Could not allocate memory in simulate.cu, aborting.\n");
        return -1.0;
    }
    double* device_curr = NULL;
    checkCudaCall(cudaMalloc((void **) &device_curr, i_max * sizeof(double)));
    if (device_curr == NULL) {
        checkCudaCall(cudaFree(device_old));
        printf("Could not allocate memory in simulate.cu, aborting.\n");
        return -1.0;
    }
    double* device_next = NULL;
    checkCudaCall(cudaMalloc((void **) &device_next, i_max * sizeof(double)));
    if (device_next == NULL) {
        checkCudaCall(cudaFree(device_old));
        checkCudaCall(cudaFree(device_curr));
        printf("Could not allocate memory in simulate.cu, aborting.\n");
        return -1.0;
    }

    /* Amount of shared memory per block: one block size plus two halo cells
     * It only makes sense to declare this if the corresponding
     * kernel function is used */
    //size_t shared_mem_amt = (threads_per_block + 2) * sizeof(double);

    cudaEvent_t start, stop;
    checkCudaCall(cudaEventCreate(&start));
    checkCudaCall(cudaEventCreate(&stop));

    /* Copy the first two generations to the GPU */
    checkCudaCall(cudaMemcpy(device_old, old,
                i_max*sizeof(double), cudaMemcpyHostToDevice));
    checkCudaCall(cudaMemcpy(device_curr, curr,
                i_max*sizeof(double), cudaMemcpyHostToDevice));

    /* Set the leftmost and rightmost entry to zero */
    checkCudaCall(cudaMemset(&device_next[0],         0, sizeof(double)));
    checkCudaCall(cudaMemset(&device_next[i_max - 1], 0, sizeof(double)));

    /* Execute kernel t_max - 2 times, excluding the first two gens.
     * CUDA doesn't provide block synchronization and using atomic add
     * operations for a global counter can lead to deadlocks. Source:
     * https://devtalk.nvidia.com/default/topic/407647/global-thread-barrier
     */
    cudaEventRecord(start, 0);

    for (int t = 2; t < t_max; ++t) {
        /* Replace the kernel launch with this line to try with shared memory,
         * with shared_mem_amt defined above
         * wave_calc_shared_kernel<<< nblocks, threads_per_block, shared_mem_amt
         */

        wave_calc_kernel<<< nblocks, threads_per_block
                        >>>(device_old, device_curr, device_next);

        /* The current kernel has copies of the pointers, therefore it is safe
         * to swap them asynchronously */
        rotate_double_ptrs(&device_old, &device_curr, &device_next);
    }

    cudaEventRecord(stop, 0);

    /* Check whether the kernel invocation was successful */
    checkCudaCall(cudaGetLastError());

    /* Copy the current array back */
    checkCudaCall(cudaMemcpy(curr, device_curr,
                i_max*sizeof(double), cudaMemcpyDeviceToHost));

    checkCudaCall(cudaFree(device_old));
    checkCudaCall(cudaFree(device_curr));
    checkCudaCall(cudaFree(device_next));

    /* Get the elapsed time for all kernel invocations */
    float elapsed_time;
    cudaEventElapsedTime(&elapsed_time, start, stop);

    return elapsed_time;
}


/* Swap three pointers; therefore function needs pointers to these pointers */
void rotate_double_ptrs (double** first_pptr,
                         double** second_pptr,
                         double** third_pptr) {
    double* temp_ptr;

    temp_ptr     = *first_pptr;
    *first_pptr  = *second_pptr;
    *second_pptr = *third_pptr;
    *third_pptr  =  temp_ptr;
}
