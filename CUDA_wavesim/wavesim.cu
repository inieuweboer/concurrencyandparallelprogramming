/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  wavesim.cu
 *
 *  Contains code for setting up and finishing the simulation.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "file.h"
#include "generatedata.h"
#include "timer.h"
#include "simulate.h"


void parse_args (int argc, char* argv[],
        double** old, double** curr, double** next,
        int* t_max, int* i_max, int* threads_per_block) {

    /* Parse commandline args: i_max t_max threads_per_block */
    if (argc < 4) {
        /* Default values, i_max a multiple of threads_per_block and
         * t_max arbitrary */
        *t_max = 50;
        *threads_per_block = 512;
        *i_max = 20 * *threads_per_block;

        printf("Usage: %s i_max t_max threads_per_block [initial_data]\n",
               argv[0]);
        printf(" - i_max (= %d): number of discrete amplitude points, "
               "must be >2\n", *i_max);
        printf(" - t_max (= %d): number of discrete timesteps, must be >=3\n",
                *t_max);
        printf(" - threads_per_block (= %d): number of threads per block "
               "to use for simulation, should be >=1\n", *threads_per_block);

        printf(" - initial_data (= sinfull): select what data will be used "
               "for the first two generations.\n");
        printf("   Available options are:\n");
        printf("    * sin: one period of the sinus function at the start.\n");
        printf("    * sinfull: entire data is filled with the sinus.\n");
        printf("    * gauss: a single gauss-function at the start.\n");
        printf("    * file <2 filenames>: allows you to specify a file with on "
               "each line a double for both generations.\n");
    }
    else {
        *i_max = atoi(argv[1]);
        *t_max = atoi(argv[2]);
        *threads_per_block = atoi(argv[3]);
    }

    if (*i_max < 3) {
        printf("argument error: i_max should be >2.\n");
        exit(EXIT_FAILURE);
    }
    if (*t_max < 3) {
        printf("argument error: t_max should be >=3.\n");
        exit(EXIT_FAILURE);
    }
    if (*threads_per_block < 1) {
        printf("argument error: threads_per_block should be >=1.\n");
        exit(EXIT_FAILURE);
    }

    /* Normalize to make sure the blocks and threads will fit precisely */
    int remainder = *i_max % *threads_per_block;
    if (remainder != 0 && *threads_per_block < *i_max) {
        *i_max -= remainder;
        printf("i_max set to %d because there is a remainder %d\n",
                *i_max, remainder);
    }
    /* It does not make sense to have more threads than points */
    else if (*threads_per_block > *i_max) {
        *threads_per_block = *i_max;
    }

    /* Allocate and initialize buffers. */
    *old     = (double*)calloc(*i_max, sizeof(double));
    *curr    = (double*)calloc(*i_max, sizeof(double));
    *next    = (double*)calloc(*i_max, sizeof(double));

    if (*old == NULL || *curr == NULL || *next == NULL) {
        fprintf(stderr, "Could not allocate enough memory, aborting.\n");
        exit(EXIT_FAILURE);
    }

    /* How should we fill our first two generations? */
    if (argc > 4) {
        if (strcmp(argv[4], "sin") == 0) {
            fill(*old, 1, *i_max/4, 0, 2*M_PI, sin);
            fill(*curr, 2, *i_max/4, 0, 2*M_PI, sin);
        } else if (strcmp(argv[4], "sinfull") == 0) {
            fill(*old, 1, *i_max-2, 0, 10*M_PI, sin);
            fill(*curr, 2, *i_max-3, 0, 10*M_PI, sin);
        } else if (strcmp(argv[4], "gauss") == 0) {
            fill(*old, 1, *i_max/4, -3, 3, gauss);
            fill(*curr, 2, *i_max/4, -3, 3, gauss);
        } else if (strcmp(argv[4], "file") == 0) {
            if (argc < 7) {
                printf("Not enough files specified!\n");
                exit(EXIT_FAILURE);
            }
            file_read_double_array(argv[5], *old, *i_max);
            file_read_double_array(argv[6], *curr, *i_max);
        } else {
            printf("Unknown initial mode: %s.\n", argv[4]);
            exit(EXIT_FAILURE);
        }
    } else {
        /* Default to full range sinus. */
        fill(*old, 1, *i_max-2, 0, 10*M_PI, sin);
        fill(*curr, 2, *i_max-3, 0, 10*M_PI, sin);
    }
}


int main (int argc, char* argv[]) {

    double time = 0.0;
    float kernel_time = 0.0;

    double* old  = NULL;
    double* curr = NULL;
    double* next = NULL;

    int t_max, i_max, threads_per_block;

    parse_args(argc, argv, &old, &curr, &next,
            &t_max, &i_max, &threads_per_block);

    timer_start();

    /* Call the actual simulation that is implemented in simulate.cu. */
    kernel_time = wave_calc_cuda(i_max, t_max, threads_per_block,
                                 old, curr, next);

    time = timer_end();
    printf("Function execution took %g secs\n", time);
    printf("Normalized: %g secs\n", time / (i_max * t_max));
    printf("Kernel invocation took %g milliseconds\n", kernel_time);
    printf("Normalized: %g msecs\n", kernel_time / (i_max * t_max));

    file_write_double_array("result.txt", curr, i_max);

    free(old);
    free(curr);
    free(next);

    return EXIT_SUCCESS;
}
