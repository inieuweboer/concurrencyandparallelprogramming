/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  generatedata.h
 *
 */

#pragma once

typedef double (*func_t)(double x);

double gauss(double x);
void fill(double *array, int offset, int range, double sample_start,
        double sample_end, func_t f);
