/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  timer.h
 *
 */

#pragma once

void timer_start(void);
double timer_end(void);
