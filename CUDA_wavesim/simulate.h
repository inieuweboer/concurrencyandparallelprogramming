/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  simulate.h
 *
 *  Header file for CUDA simulation code.
 *
 */

#define C 0.15  /* Spatial impact */

static void checkCudaCall (cudaError_t);

__device__ double calc_point (int, double*, double*);
__global__ void wave_calc_kernel (double*, double*, double*);
__global__ void wave_calc_shared_kernel (double*, double*, double*);

float wave_calc_cuda (int, int, int, double*, double*, double*);

void rotate_double_ptrs (double**, double**, double**);
