/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  file.h
 *
 */

#pragma once

void file_read_double_array(const char* filename, double* array, int n);
void file_write_double_array(const char* filename, double* array, int n);
